
import React from 'react'
import Snowfall from 'react-snowfall';

export interface SnowProps {
    isActive: boolean,
}

const Snow: StorefrontFunctionComponent<SnowProps> = ({ isActive }) => {

    if (!isActive) {
        return null;
    }
    return (
        <div style={{ height: '100%', width: '100%', position: 'absolute' }}>
            <Snowfall color="white" style={{ zIndex: 1, position: 'fixed' }}></Snowfall>
        </div>
    )
}

Snow.schema = {
    title: 'Editor de nieve',
    description: 'Editor de nieve',
    type: 'object',
    properties: {
        isActive: {
            title: 'Activar nieve?',
            description: 'Activar o desactivar el editor de nieve',
            type: 'boolean',
            default: false,
        }
    }

}

// ContactPage.schema = {
//   title: 'Pagina de Contactos',
//   description: 'Modificadores de la pagina de contactos',
//   type: 'object',
//   properties: {
//     urlFacebook: {
//       title: 'Url de Facebook ',
//       description: 'Url de Facebook de Ganacell',
//       type: 'string',
//       default: 'https://www.facebook.com/GanaCellEcuador',
//     },
//     urlInstagram: {
//       title: 'Url de Instagram ',
//       description: 'Url de Instagram de Ganacell',
//       type: 'string',
//       default: 'https://www.instagram.com/ganacell_ecuador/',
//     },
//     urlWhatsapp: {
//       title: 'Url de Whatsapp ',
//       description: 'Url de Whatsapp de Ganacell',
//       type: 'string',
//       default: 'https://api.whatsapp.com/send?phone=+593979166156',
//     },
//   },
// }

export default Snow 
