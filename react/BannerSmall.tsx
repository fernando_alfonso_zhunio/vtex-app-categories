/* eslint-disable prettier/prettier */
/* eslint-disable no-console */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from 'react'
// import stylesBannerSmall from './stylesBannerSmall.css';
import queryProduct from './graphql/getProduct.gql'
import { useQuery } from 'react-apollo'
import { Spinner } from 'vtex.styleguide'
import { useCssHandles } from 'vtex.css-handles';
import { useRuntime } from 'vtex.render-runtime';

export interface BannerSmallProps {
  isActive: boolean,
  objectoDateAndSku: any
}
const BannerSmall: StorefrontFunctionComponent<BannerSmallProps> = ({ isActive, objectoDateAndSku }) => {

  if (!isActive) {
    return null;
  }
  if (objectoDateAndSku) {
    objectoDateAndSku = JSON.parse(objectoDateAndSku)
  }

  var now = new Date()
  const dayAndSku: object = objectoDateAndSku ? objectoDateAndSku : {
    '2021-06-09': '1147',
    '2021-06-10': '27',
    '2021-06-11': '1147',
    '2021-06-12': '1147',
    '2021-06-13': '27',
    '2021-06-14': '1147',
    '2021-06-15': '27',
    '2021-06-16': '1147',
    '2021-06-17': '1147',
    '2021-06-18': '1147',
    '2021-06-19': '1147',
    '2021-06-20': '27',
    '2021-06-21': '27',
    '2021-06-25': '27',
    '2021-06-26': '27',
    '2021-06-27': '27',
    '2021-06-28': '27',
    '2021-06-29': '27',
    '2021-06-30': '27',
    '2021-07-02': '27',
    '2021-07-08': '27',
    '2021-07-03': '27',
    '2021-07-04': '27',
    '2021-07-05': '27',
    '2021-07-06': '27',
    '2021-08-31': '27',
  }


  const [fullDay] = useState(now.toISOString().slice(0, 10));
  const [dateNow]: any = useState(dayAndSku);
  const { navigate } = useRuntime()

  if (!dayAndSku[fullDay]) {
    // return <div></div>
    return <span style={{ display: "none" }}>sin contrareloj</span>
  }

  const { data, loading, error } = useQuery(queryProduct, {
    variables: { identifier: { value: dateNow[fullDay], field: 'sku' } },
  })

  const [minutes, setMinutes] = useState(60 - now.getMinutes())
  const [seconds, setSeconds] = useState(60 - now.getSeconds())
  const [hours, setHours] = useState(23 - now.getHours())
  const CSS_HANDLES = [
    'alignCenter',
    'times',
    'time',
    'textTime',
    'grid-banner-small-main',
    'grid-banner-small',
    'textAlign',
    'textFlash',
    'btnOferta',
    'firstContainer',
    'secondContainer',
    'thirdContainer',
    'secondContainerContent',
    'textName',
    'imgOferta',
    'priceOfert',
    'secondParent',
  ]
  const styles = useCssHandles(CSS_HANDLES)
  useEffect(() => {
    const interval = setInterval(() => {
      if (seconds - 1 < 0) {
        setSeconds(60)
        if (minutes - 1 < 0) {
          setMinutes(60)
          if (hours - 1 < 0) {
            setHours(24)
          }
          setHours(hours => hours - 1)
        }
        setMinutes(minutes => minutes - 1)
      }
      setSeconds(old => old - 1)
    }, 1000)
    return () => clearInterval(interval)
  })
  if (loading) return <Spinner />
  if (error) return <div>{error}</div>
  function redirectProduct(href: string) {
    navigate({
      to: href + '/p',
    })
  }
  return (
    <div className={`${styles['grid-banner-small-main']}`}>
      <div className={`${styles['grid-banner-small']}`}>
        <div className={`${styles.textAlign} ${styles.firstContainer}`}>
          <div>
            <h3 className={`${styles.textFlash} t-heading-1 ma2`}>
              Venta Flash
            </h3>
            <h4 className={`ma2`}>Oferta disponible solo hoy</h4>
            <h5 className={`ma2`}>Solo en novicompu.com</h5>
          </div>
        </div>
        <div className={`${styles.secondContainer}`}>
          <div className={`${styles.secondParent}`}>
            <div className={`${styles.secondContainerContent} pa4`}>
              <h4 className={`${styles.textName} ma2`}>
                {data.product.items[0].nameComplete}
              </h4>
              <h4 className={`${styles.name} ma2`}>
                De {(data.product.priceRange.listPrice.highPrice * 1.12).toFixed(2)}
              </h4>
              <h4 className={`${styles.priceOfert} ma2`}>
                Oferta {(data.product.priceRange.sellingPrice.highPrice * 1.12).toFixed(2)}
              </h4>
              <button
                onClick={() => {
                  redirectProduct(data.product.linkText)
                }}
                // href={data.product.linkText + '/p'}
                className={`${styles.btnOferta}`}
              >
                Ver Oferta
              </button>
            </div>
            <div className={`${styles.alignCenter}`}>
              <img
                onClick={() => {
                  redirectProduct(data.product.linkText)
                }}
                src={data.product.items[0].images[0].imageUrl}
                className={`${styles.imgOferta}`}
              />
            </div>
          </div>
        </div>
        <div className={`${styles.alignCenter} ${styles.thirdContainer}`}>
          <div className={`${styles.alignCenter}`}>
            <div className={`${styles.times}`}>
              <span className={`${styles.time}`}>{hours}</span>
              <span className={`${styles.time}`}>{minutes}</span>
              <span className={`${styles.time}`}>{seconds}</span>
              <span className={`${styles.textTime}`}>H</span>
              <span className={`${styles.textTime}`}>M</span>
              <span className={`${styles.textTime}`}>S</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

BannerSmall.schema = {
  title: 'Banner de contrareloj',
  description: 'Pagina de productos de la Ganacell',
  type: 'object',
  properties: {
    isActive: {
      type: 'boolean',
      default: true,
      description: "Cambia el estado del banner contrareloj de activo a desactivado y viceversa",
      title: "Activar o Desactivar Banner"
    },
    objectoDateAndSku: {
      type: 'string',
      description: "Objeto string de dia y sku",
      title: 'Cadena de Fecha y Sku',
      default:
        ` {
        "2021-06-09": "1147",
        "2021-06-10": "27",
        "2021-06-11": "1147",
        "2021-06-12": "1147",
        "2021-06-13": "27",
        "2021-06-14": "1147",
        "2021-06-15": "27",
        "2021-06-16": "1147",
        "2021-06-17": "1147",
        "2021-06-18": "1147",
        "2021-06-19": "1147",
        "2021-06-20": "27",
        "2021-06-21": "27",
        "2021-06-25": "27",
        "2021-06-26": "27",
        "2021-06-27": "27",
        "2021-06-28": "27",
        "2021-06-29": "27",
        "2021-06-30": "1147",
        '2021-07-02': '1147',
    '2021-07-08': '27',
    '2021-07-03': '27', 
    '2021-07-04': '27',
    '2021-07-05': '27',
    '2021-07-06': '27',
    '2021-07-07': '27',
      }`
    }
  }
}

export default BannerSmall
