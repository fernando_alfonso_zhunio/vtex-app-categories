import React, { useState } from 'react'
import styles from './ItemMenuMobile.css'
import { Icon } from 'vtex.store-icons'
import { useRuntime } from 'vtex.render-runtime'
// import { useCssHandles } from 'vtex.css-handles'

// interface IItemMenu {
//   href: string;
//   hasChildren: boolean;
//   name: string;
// }

const ItemMenuMobile = ({ item, onChangeOpenOrClose }) => {
  const [isOpenMenu, setIsOpenMenu] = useState(false)
  // const [openOrClose, setOpenOrClose] = useState(false)

  const { navigate } = useRuntime()

  // function openOrCloseSubMenu(hasChildren: boolean, href: string) {
  //   if (!hasChildren) {
  //     navigate({
  //       to: href,
  //     })
  //     openOrCloseMenu();
  //   }
  // }

  function openOrCloseMenu() {
    // setOpenOrClose(!openOrClose)
    onChangeOpenOrClose(false)
  }
  function goCategory(href: string) {
    navigate({
      to: href,
    })
    openOrCloseMenu()
  }

  function clearAccent(text) {
    const textModify = text.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
    console.log(textModify.toLowerCase()) 
    return textModify.toLowerCase()
  }

  return (
    <li className="pa0">
      <div className={`${styles['item-content']}`}>
        <span
          className={`${styles[clearAccent(item.name)]}`}
          onClick={() => {
            goCategory(item.href)
          }}
        >
          {item.name}
        </span>
        {item.hasChildren && (
          <button onClick={() => setIsOpenMenu(!isOpenMenu)}>
            <Icon
              id={`nav-caret--${isOpenMenu ? 'up' : 'down'}`}
              activeClassName="rebel-pink"
            />
          </button>
        )}
      </div>
      {item.hasChildren && (
        <ul
          className={`${isOpenMenu ? styles['open-child'] : styles['close-child']
            }`}
        >
          {item.children.map((itemChild: any, subindex: number) => {
            return (
              <li key={`sub-${subindex}`}>
                <span
                  onClick={() => {
                    goCategory(itemChild.href)
                  }}
                >
                  {itemChild.name}
                </span>
              </li>
            )
          })}
        </ul>
      )}
    </li>
  )
}

export default ItemMenuMobile