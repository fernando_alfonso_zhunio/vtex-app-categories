import React from 'react'

const termCond: StorefrontFunctionComponent<{}> = () => {
  return (
    <div className="center pl5 pr5 pl10-m pr10-m tj lh-copy">
      <h2 className="t-heading-1 tc c-action-primary">Términos generales</h2>
      <p>
        Esta garantía limitada de hardware comercializado por NOVISOLUTIONS CIA.
        LTDA. le concede a usted, el cliente, los derechos de Garantía Limitada
        otorgados expresamente por NOVISOLUTIONS CIA. LTDA, quien es
        comercializador de productos. Asimismo, los clientes pueden tener otros
        derechos legales en virtud de cualquier acuerdo especial celebrado con
        NOVISOLUTIONS CIA. LTDA. por escrito. NOVISOLUTIONS CIA. LTDA. no ofrece
        ninguna otra condición o garantía expresa, ya sea de forma oral o
        escrita expresamente a todas las garantías y condiciones no establecidas
        en la presente garantía. <br />
        Esta garantía limitada tiene efecto en todas las provincias o regiones
        del Ecuador y se solicitará su cumplimiento en la ciudad de Quito,
        capital del Ecuador y en la ciudad de Guayaquil. Dentro del programa de
        garantía limitada de NOVISOLUTIONS CIA. LTDA., los productos adquiridos
        en una provincia o región del Ecuador pueden transferirse a otras
        latitudes, sin que se anule la misma. <br />
        Los términos de la garantía, la disponibilidad del servicio y los
        tiempos de respuesta del mismo pueden variar de una provincia a otra o
        de una región a otra. El tiempo estándar de respuesta del servicio de
        garantía está sujeto a cambios debido a la disponibilidad de piezas a
        nivel local. El servicio técnico de NOVISOLUTIONS CIA. LTDA. O del socio
        estratégico que se escoja, establecerá ese tiempo en el diagnóstico que
        se le haga al equipo que requiera el servicio o que ejecuta esta
        Garantía Limitada. <br />
        NOVISOLUTIONS CIA. LTDA. no se hace responsable de los gastos que
        demanden ejecutar esta Garantía limitada, como los que pueden conllevar 
        el traslado de los productos hasta la Ciudad de Quito, lugar donde se
        ejecuta la Garantía Limitada. <br />
        Esta Garantía Limitada sólo es aplicable a productos de hardware
        comercializados por NOVISOLUTIONS CIA. LTDA. (cuestión que se demostrará
        a través de la factura emitida por sus locales comerciales y
        distribuidores autorizados) acompañados del original del presente
        documento. <br />
        El término “Producto de hardware comercializado por NOVISOLUTIONS CIA.
        LTDA.” sólo hace referencia a los componentes de hardware y al firmware
        requerido. El término “Producto de hardware comercializado por
        NOVISOLUTIONS CIA. LTDA.” NO incluye ninguna aplicación de software, ni
        programas, productos que no sean comercializados por NOVISOLUTIONS CIA.
        LTDA. <br />
        NOVISOLUTIONS CIA. LTDA. garantiza que los “Producto de hardware
        comercializado por NOVISOLUTIONS CIA. LTDA.” no presentaran ningún
        defecto en sus piezas ni en su acabado bajo condiciones de uso normal en
        el período de garantía limitada. <br />
        El período de garantía limitada comienza en la fecha de compra. La
        Factura de compra, donde se muestra la fecha de adquisición del
        producto, constituye la prueba de la fecha de compra. Es obligación
        presentar dicha prueba de compra para recibir un servicio de garantía.
        El usuario podrá percibir los servicios de garantía del hardware según
        los términos y condiciones especificados en este documento en caso de
        que sea necesario reparar el producto de hardware comercializado por
        NOVISOLUTIONS CIA. LTDA. dentro del período de garantía limitada. <br />
        A menos que se indique lo contrario los productos comercializados por
        NOVISOLUTIONS CIA. LTDA., pueden ser reparados con materiales nuevos, o
        con materiales nuevos y usados equivalentes a los nuevos en cuanto a
        rendimiento y fiabilidad. NOVISOLUTIONS CIA. LTDA. puede reparar o
        sustituir los productos de hardware comercializados por NOVISOLUTIONS
        CIA. LTDA. <br />
        Con productos o piezas nuevos o previamente utilizados equivalentes a
        los nuevos en cuanto a rendimiento y fiabilidad, o Con productos
        equivalentes a un producto original que haya dejado de fabricarse.{' '}
        <br />
        Se garantiza que las piezas de repuesto no presentan defectos en sus
        materiales ni en su acabado por un período de noventa (90) días o por el
        resto del período de garantía limitada del producto de hardware
        comercializado por NOVISOLUTIONS CIA. LTDA. al que reemplacen o en el
        que estén instaladas, por el período que sea más extenso. <br />
        Durante el período de garantía limitada, NOVISOLUTIONS CIA. LTDA.
        reparará o reemplazará cualquier componente defectuoso a su criterio.
        Todas las piezas o los productos de hardware que se extraigan bajo esta
        garantía limitada serán propiedad de NOVISOLUTIONS CIA. LTDA. En el caso
        improbable de que el Producto de hardware comercializado por
        NOVISOLUTIONS CIA. LTDA. funcione incorrectamente de forma recurrente,
        NOVISOLUTIONS CIA. LTDA., según su criterio, puede optar por
        proporcionar al usuario Una unidad de repuesto a elección de
        NOVISOLUTIONS CIA. LTDA. que sea igual o equivalente al Producto de
        hardware comercializado por NOVISOLUTIONS CIA. LTDA. en rendimiento; o,
        Reembolsarle el precio de compra (menos los intereses) a través de una
        “Nota de Crédito”, en lugar de sustituir el producto. Éste es el único
        recurso disponible para los productos defectuosos.
      </p>
      <h3>EXCLUSIONES</h3>
      <p>
        Los abajo firmantes reconocen y aceptan que los equipos electrónicos
        requieren un mantenimiento apropiado de manera periódica; y, que el
        mantenimiento apropiado se lo debe hacer en el servicio técnico
        autorizado. <br />
        NOVISOLUTIONS CIA. LTDA. no garantiza que el funcionamiento de los
        productos comercializados se producirá sin interrupciones ni errores.
        NOVISOLUTIONS CIA. LTDA. no se hace responsable de los daños producidos
        como consecuencia del incumplimiento de las instrucciones que acompañan
        al producto de hardware comercializado por NOVISOLUTIONS CIA. LTDA.{' '}
        <br />
        Esta Garantía Limitada no se aplica a piezas que pueden gastarse o
        consumirse y no se extiende a ningún producto al que se le haya extraído
        el número de serie o haya sufrido daños, o haya quedado defectuoso:{' '}
        <br />
        <ul>
          <li>
            Como consecuencia de un accidente, mala utilización, abuso,
            contaminación, mantenimiento o calibración impropia o inadecuada, o
            cualquier otro factor externo.{' '}
          </li>
          <li>
            Por utilización bajo parámetros diferentes a los establecidos en la
            documentación de usuario incluida con el producto.
          </li>
          <li>
            Por uso de software, interfaces piezas o repuestos no suministrados
            por NOVISOLUTIONS CIA. LTDA.
          </li>
          <li>Por mantenimiento o preparación inadecuados del sitio.</li>
          <li>Por infección de virus.</li>
          <li>Por pérdida o daños de transporte.</li>
          <li>
            Por modificación o servicio externos a NOVISOLUTIONS CIA. LTDA., de
            la misma forma, la instalación que el usuario realice del producto
            reemplazable o piezas aprobadas para el producto comercializado,
            disponibles en cualquier almacén.
          </li>
        </ul>
      </p>
      <h3>
        RECOMENDACIONES QUE DEBEN SER OBSERVADAS EN EL MOMENTO DE EJECUTAR LA
        GARANTÍA LIMITADA
      </h3>
      <p>
        Debe realizar copias de seguridad periódicas de los datos almacenados en
        el disco duro o en otros dispositivos de almacenamiento como precaución
        ante posibles fallos, alteraciones o pérdidas de datos. Antes de
        devolver una unidad para su reparación, asegúrese de realizar una copia
        de seguridad de los datos y de eliminar cualquier tipo de información
        confidencial o de carácter privado o personal. <br />
        NOVISOLUTIONS CIA. LTDA. no se responsabiliza de los daños o pérdidas de
        programas, datos o medios de almacenamiento extraíbles. <br />
        NOVISOLUTIONS CIA. LTDA. no se responsabiliza de la restauración o
        reinstalación de programas o datos distintos al software instalado al
        momento de comercializar el producto. Los datos de las unidades que se
        envíen a reparación pueden haberse eliminado del disco duro y los
        programas pueden estar en su estado original. <br />
      </p>
      <h3>RECURSOS EXCLUSIVOS</h3>
      <p>
        Estos términos y condiciones constituyen el contrato de garantía
        completo exclusivo entre usted y NOVISOLUTIONS CIA. LTDA., respecto del
        producto de hardware comercializado por NOVISOLUTIONS CIA. LTDA. y
        adquirido por usted. Por lo tanto, estos términos y condiciones
        reemplazan cualquier acuerdo o declaración previos, incluidos las
        declaraciones de la documentación de venta de NOVISOLUTIONS CIA. LTDA. o
        los consejos proporcionados al usuario por NOVISOLUTIONS CIA. LTDA. a
        través de un agente o empleado de NOVISOLUTIONS CIA. LTDA., en relación
        con la compra del producto del hardware comercializado por NOVISOLUTIONS
        CIA. LTDA.. Los cambios de las condiciones de esta garantía limitada no
        serán válidos a menos que se establezcan por escrito y los firme el
        GERENTE DE NOVISOLUTIONS CIA. LTDA.
      </p>
      <h3>LIMITACIÓN DE RESPONSABILIDAD</h3>
      <p>
        Si el producto de hardware comercializado por NOVISOLUTIONS CIA. LTDA.
        no funciona de la manera establecida anteriormente, la responsabilidad
        máxima de NOVISOLUTIONS CIA. LTDA. bajo esta Garantía Limitada se reduce
        expresamente al reembolso del precio menor de entre el precio abonado
        por el producto o el pago del coste de la reparación o sustitución de
        cualquiera de los componentes de hardware cuyo funcionamiento sea
        anómalo bajo condiciones de uso normal. <br />
        Excepto en los casos indicados anteriormente, NOVISOLUTIONS CIA. LTDA.
        no se hace responsable en ningún caso de los daños causados por el
        producto o por la falta de funcionamiento del mismo, incluidas las
        pérdidas de beneficios empresariales o los daños especiales,
        incidentales o consecuentes. <br />
        NOVISOLUTIONS CIA. LTDA. no se hace responsable de ninguna reclamación
        efectuada por terceros o por el usuario en representación de terceros.{' '}
        <br />
        Esta limitación de responsabilidad se aplica tanto en el caso de que se
        produzcan daños bajo esta garantía o como responsabilidad
        extracontractual (incluyendo responsabilidad absoluta del producto y
        negligencia), responsabilidad contractual o de cualquier otro tipo.
        Ninguna persona puede renunciar a esta limitación de responsabilidad o
        modificarla. Esta limitación de responsabilidad será efectiva incluso en
        el caso de haber notificado a NOVISOLUTIONS CIA. LTDA. o a un
        representante autorizado de NOVISOLUTIONS CIA. LTDA. la posibilidad de
        tales daños. <br />
      </p>
      <h3>PERÍODO DE GARANTÍA LIMITADA</h3>
      <p>
        El Período de garantía limitada de un producto de hardware
        comercializado por NOVISOLUTIONS CIA. LTDA. hace referencia al período
        específico establecido que comienza en la fecha de su adquisición. La
        fecha de la Factura de compra es la fecha de adquisición a menos que
        NOVISOLUTIONS CIA. LTDA. o su distribuidor le informen de lo contrario
        por escrito.
      </p>
      <h3>SERVICIOS DE GARANTÍA</h3>
      <p>
        Para que NOVISOLUTIONS CIA. LTDA. pueda ofrecer el mejor servicio y
        asistencia durante el periodo de garantía limitada, siga las
        recomendaciones de la casa representante de los productos
        comercializados por NOVISOLUTIONS CIA. LTDA., para verificar
        configuraciones, cargar el firmware más reciente, instalar parches de
        software, realizar pruebas de diagnóstico o utilizar soluciones de
        asistencia remotas de los productos comercializados por NOVISOLUTIONS
        CIA. LTDA. cuando sea necesario. <br />
        NOVISOLUTIONS CIA. LTDA. recomienda el uso de las tecnologías de
        asistencia disponibles que suministran las firmas de productos
        comercializados por NOVISOLUTIONS CIA. LTDA. Si decide no utilizar las
        funciones disponibles de servicio técnico remoto, es posible que incurra
        en gastos adicionales debido al aumento de los requisitos de los
        recursos de servicio técnico. <br />
        A continuación, se enumeran los servicios de garantía técnica que NO
        pueden aplicarse al producto de hardware comercializado por
        NOVISOLUTIONS CIA. LTDA., que usted ha adquirido. <br />
        <ul>
          <li>Servicio de garantía de auto reparación por el cliente.</li>
          <li>Servicio de garantía de retiro y devolución.</li>
          <li>Servicio de garantía en taller</li>
          <li>Actualizaciones del servicio</li>
        </ul>
        Excepto en los casos en que se especifique en cualquier licencia de
        usuario final de software o en acuerdos de licencia de programas, o si
        así lo requieren las leyes locales, el software y el contenido digital
        de terceros (incluido el sistema operativo y cualquier software o
        contenido digital de terceros preinstalado en los productos
        comercializados por NOVISOLUTIONS CIA. LTDA.) se entregan “tal cual”.
        Excepto en los casos en que se indique específicamente en esta garantía
        limitada, en ningún caso NOVISOLUTIONS CIA. LTDA. será responsable de
        los daños derivados de o provocados por el contenido digital de terceros
        preinstalado en el producto de hardware comercializado por NOVISOLUTIONS
        CIA. LTDA.
      </p>
      <h3>TERMINOS Y CONDICIONES DE USO</h3>
      <h4>DESTINO</h4>
      <p>
        Para destinos dentro de la ciudad de Guayaquil, el despacho de su pedido
        será dentro de 48 horas o previa confirmación de nuestra pasarela de
        pago. Para destinos fuera de la ciudad, el tiempo de entrega dependerá
        de la distancia del destino después de 48H hasta un máximo de 96H.
      </p>
      <h4>DESPACHO</h4>
      <p>
        Todo despacho será agendado después de que el pago de su pedido sea
        confirmado por nuestra pasarela de pago. En caso de tener un error en su
        dirección, nos intentaremos comunicar con la persona registrada para
        confirmar sus datos de facturación, contacto de quien recibe y
        confirmación de los productos pedidos. Las entregas se realizan en la
        dirección brindada en horarios de 8 AM a 6 PM.
      </p>
      <h4>REEMBOLSO</h4>
      <p>
        Los reembolsos pueden tardar de 30 a 90 días laborables ya que se
        realiza entre agencias bancarias y NOVISOLUTIONS CIA. LTDA. Si el
        reembolso no es aplicado el mismo día con corte de DATAFAST o MEDIANET,
        el cliente deberá esperar el tiempo estipulado en el punto anterior y
        adicional se descontará la comisión bancaria del 10%.
      </p>
      <h4>CONDICIONES DE GARANTÍA Y REEMBOLSO</h4>
      <p>
        Todos nuestros productos tienen garantía. La misma es la garantía que el
        fabricante ofrece por sus productos. La Garantía comienza desde la fecha
        de facturación (no desde la fecha de entrega del producto). El periodo
        de cobertura es de acuerdo a la tabla de las políticas de nuestra
        empresa que se enseña a continuación, sin embargo, existen productos con
        menor tiempo y de igual manera con menor valor que no constan con
        garantía. Para mayor información acercarse a una tienda para consultar
        el periodo de garantía de su producto. <br />
        Si un producto se daña después de finalizado su periodo de garantía no
        aplica la garantía. NOVISOLUTIONS CIA. LTDA. realiza cambios de
        productos cuando no se puede rehabilitar el producto, o cuando la
        rehabilitación demore más de 30 días o cuando el costo de rehabilitar el
        artículo supera el costo del producto. <br />
        Para solicitar la garantía el producto debe estar acompañado por la
        factura original (sin alteraciones). <br />
        La garantía cubre defectos de fabricación que ocurran bajo el uso normal
        del producto (uso doméstico, no comercial ni industrial). No cubre daños
        físicos ni golpes. El daño físico en un producto anula automáticamente
        la garantía <br />
      </p>
    </div>
  )
}

// termCond.schema = {
//   title: 'MyComponent Title',
//   description: 'MyComponent description',
//   type: 'object',
//   properties: {
//     someString: {
//       title: 'SomeString Title',
//       description: 'editor.my-component.someString.description',
//       type: 'string',
//       default: 'SomeString default value',
//     },
//   },
// }

export default termCond
