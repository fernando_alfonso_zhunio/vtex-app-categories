import React from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { useRuntime } from 'vtex.render-runtime'

interface IpropsContact {
  urlFacebook: string
  urlInstagram: string
  urlWhatsapp: string
}
const ContactPage: StorefrontFunctionComponent<IpropsContact> = ({
  urlFacebook,
  urlInstagram,
  urlWhatsapp,
}) => {
  const CSS_HANDLES = [
    'page-contact',
    'contact-title',
    'contact-shop',
    'contact-shop-title',
    'contact-shop-wrapper',
    'contact-shop-btn',
    'contact-img-shop',
    'contact-redes',
    'contact-redes-wrapper',
    'contact-red-social',
    'contact-redes-wrapper',
    'contact-redes-content',
    'contact-red-img-social',
  ] as const
  const { navigate } = useRuntime()

  const styles = useCssHandles(CSS_HANDLES)
  function goTiendas() {
    navigate({
      to: '/tiendas',
    })
  } 
  return (
    <div className={`${styles['page-contact']}`}>
      <h1 className={`${styles['contact-title']} t-heading-1`}>Contactos</h1>
      <div>
        <div className={`${styles['contact-shop']}`}>
          <div>
            <div className={`${styles['contact-shop-wrapper']}`}>
              <div>
                <h2 className={`${styles['contact-shop-title']}`}>Tiendas</h2>
                <p>
                  Mira tu tiendas màs cerca y visitanos para ver todas nuestras
                  ofertas
                </p>
                <button
                  onClick={() => {
                    goTiendas()
                  }}
                  className={`${styles['contact-shop-btn']}`}
                >
                  Mirar todas las tiendas
                </button>
              </div>
              <img
                src="/arquivos/shop.svg"
                className={`${styles['contact-img-shop']}`}
                alt="tiendas Novicompu"
              />
            </div>
          </div>
        </div>
        <div className={`${styles['contact-redes-wrapper']}`}>
          <h2>Redes Sociales</h2>
          <div className={`${styles['contact-redes-content']}`}>
            <div className={`${styles['contact-red-social']}`}>
              <a className="link" target="_blank" href={urlFacebook}>
                <img
                  className={`${styles['contact-red-img-social']}`}
                  src="/arquivos/facebook.svg"
                  alt="facebook Novicompu"
                  title="facebook Novicompu"
                />
                <h4>Facebook</h4>
              </a>
            </div>
            <div className={`${styles['contact-red-social']}`}>
              <a className="link" target="_blank" href={urlInstagram}>
                <img
                  className={`${styles['contact-red-img-social']}`}
                  src="/arquivos/instagram.svg"
                  alt="instagram Novicompu"
                  title="instagram Novicompu"
                />
                <h4>Instagram</h4>
              </a>
            </div>
            <div className={`${styles['contact-red-social']}`}>
              <a className="link" target="_blank" href={urlWhatsapp}>
                <img
                  className={`${styles['contact-red-img-social']}`}
                  src="/arquivos/whatsapp.svg"
                  alt="whatsapp Novicompu"
                  title="whatsapp Novicompu"
                />
                <h4>whatsapp</h4>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

ContactPage.schema = {
  title: 'Pagina de Contactos',
  description: 'Modificadores de la pagina de contactos',
  type: 'object',
  properties: {
    urlFacebook: {
      title: 'Url de Facebook ',
      description: 'Url de Facebook de Novicompu',
      type: 'string',
      default: 'https://www.facebook.com/novicompu/',
    },
    urlInstagram: {
      title: 'Url de Instagram ',
      description: 'Url de Instagram de Novicompu',
      type: 'string',
      default: 'https://www.instagram.com/novicompu/',
    },
    urlWhatsapp: {
      title: 'Url de Whatsapp ',
      description: 'Url de Whatsapp de Novicompu',
      type: 'string',
      default: 'https://api.whatsapp.com/send?phone=+593939749166',
    },
  },
}

export default ContactPage
