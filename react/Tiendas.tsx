/* eslint-disable react/jsx-key */
/* eslint-disable prettier/prettier */
import React from 'react'
// import { injectIntl } from 'react-intl'

// import { useCssHandles } from 'vtex.css-handles'
// import { useRuntime } from 'vtex.render-runtime'
import stylesGlobals from './globals.css'
import { Icon } from 'vtex.store-icons'


export interface TiendasPageProps {
  tiendas: string;
}
const TiendasPage: StorefrontFunctionComponent<TiendasPageProps> = ({ tiendas }) => {

  try {
    if (tiendas) tiendas = JSON.parse(tiendas);
  }
  catch (ex) {
    console.error("El props de tienda debe ser un string con formato a json: ", ex)
  }

  const tiendasJson =
    (typeof tiendas == 'object') ? tiendas :
    [
      {
        "Guayaquil": [
          {
            "name": "HIPER NOVICOMPU",
            "address": "KENNEDY NORTE AV. FRANCISCO DE ORELLANA N.30 Y NAHIN ISAIAS  MZ 71 (ALADO DE LA PARRILLADA DEL ÑATO)",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU FRANCISCO DE ORELLANA",
            "address": "CIUDADELA KENNEDY NORTE AV. FRANCISCO ORELLANA SOLAR 5 Y EUGENIO ALMAZAN (JUNTO A JUAN MARCET)",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU 9 DE OCTUBRE",
            "address": "EDIF KFC: 09 DE OCTUBRE 910 Y RUMICHACA JUNTO A EDIF MILITARES",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU 9 DE OCTUBRE 2",
            "address": "09 DE OCTUBRE 515 Y ESCOBEDO LOCAL 2B",
            "telefono": null,
            "days": null
          },
          {
            "name": "CITY MALL",
            "address": "C.C. CITY MALL: FELIPE PESO S/N BENJAMIN CARRION LOCAL 5",
            "telefono": null,
            "days": null
          },
          {
            "name": "MALL DEL SUR",
            "address": "C.C. MALL DEL SUR: AV 25 DE JULIO JOSE DE LA CUADRA LOCAL 26",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU URDESA",
            "address": "AV. VICTOR EMILIO ESTRADA N 431 Y EBANOS",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU URDESA 2",
            "address": "VICTOR EMILIO ESTRADA 612A MONJAS Y FICUS JUNTO A PHARMACYS",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "Quito": [
          {
            "name": "NOVICOMPU CARACOL",
            "address": "C.C. CARACOL: AV AMAZONAS Y AV NNUU LOCAL 108 (FRENTE AL CC IÑAQUITO)",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU EL RECREO",
            "address": "C.C. EL RECREO: PEDRO VICENTE MALDONADO S11-122 EL RECREO K15",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU NACIONES UNIDAS",
            "address": "AV. NNUU Y SHYRIS PB (JUNTO A BCO BOLIVARIANO)",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPULOS SHYRIS ",
            "address": "AV LOS SHYRIS N44-26 AV RIO COCA LOCAL 6 A UNA CUADRA DE POLLOS DE LA KENEDY ",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU 6 DE DICIEMBRE",
            "address": "AV 06 DE DICIEMBRE Y COLON LT 2016 N26 BATALLAS ESQ LOCAL 3 ",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU QUITUMBE",
            "address": "AV MAESTRO OE4-202 QUITUMBE ",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU EL RECREO 2",
            "address": "C.C. RECREO PEDRO VICENTE MALDONADO MALADONADO S10-194 CALVAS JUNTO AMERICA CLASICC",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU REPUBLICA",
            "address": "AV REPUBLICA N36-148 Y AV AMERICA ESQUINA",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU 9 DE AGOSTO",
            "address": "9 DE AGOSTO Y LIZARDO BECERRA, DIAGONAL AL DILIPA",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU RUMIÑAHUI",
            "address": "RUMIÑAHUI E ISLA MARCHANA SECTOR SAN RAFAEL. JUNTO A LA GASOLINERA TERPEL",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU CARAPUNGO",
            "address": "CALLE CARAPUNGO OE4-173 Y PANAMERICANA NORTE, VIA PRINCIPAL CALDERON",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU SANGOLQUI",
            "address": "AV. GRAL ENRIQUEZ #2814 SECTOR REDONDEL RIVER MALL ",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU ",
            "address": "ALONSO DE TORRES N44 -98  Y EDMUNDO CARVAJAL DIAGONAL AL C.C EL BOSQUE",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "Cuenca": [
          {
            "name": "NOVICOMPU MALL DEL RIO",
            "address": "C.C. MALL DEL RIO AV FELIPE CIRCUNVALACIÓN SUR PLANTA BAJA LOCAL S3",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU MALL DEL RIO 2",
            "address": "C.C. MALL DEL RIO FELIPE II Y CIRCUNVALACIÓN SUR  PLANTA BAJA LOCAL C 5-6-7-8-9-10",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "Ambato": [
          {
            "name": "NOVICOMPU AMBATO 1",
            "address": "LALAMA O8-59 ENTRE LAS CALLES JUAN BENIGNO VELA Y AV CEVALLOS",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU AMBATO 2",
            "address": "AV. PEDRO FERMIN CEVALLOS Y JOAQUIN AYLLON",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "Manta": [
          {
            "name": "NOVICOMPU ",
            "address": "Manta calle TRECE SN avenida 12/13",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU ",
            "address": "MANTA CALLE AV 2 #1175 JUNTO AL BCO PICHINCHA",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "Chone": [
          {
            "name": "NOVICOMPU CHONE",
            "address": "CALLE ROCAFUERTE ENTRE COLON Y PICHINCHA",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "Portoviejo": [
          {
            "name": "NOVICOMPU PORTOVIEJO 1",
            "address": "AV MANABI ENTRE PIO MONTUFAR Y ATAHUALPA (DIAGONAL KILON COMIDAS CHINAS)",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU PORTOVIEJO 1",
            "address": "PORTOVIEJO: P GUAL 921 Y GARCIA MORENO EDIF ALVERTO GILER DIAGONAL A ORVE HOGAR",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "Machala": [
          {
            "name": "NOVICOMPU MACHALA ",
            "address": "ROCAFUERTE ENTRE GUAYAS Y AYACUCHO, JUNTO A CORTE DE JUSTICIA",
            "telefono": null,
            "days": null
          },
          {
            "name": "NOVICOMPU BUENAVISTA",
            "address": "BUENAVISTA ENTRE BOLIVAR Y ROCAFUERTE DIAGONAL AL COMERCIO",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "SANTO DOMINGO": [
          {
            "name": "NOVICOMPU SANTO DOMINGO ",
            "address": "Av. Quito 1308 y Abraham Calazacom",
            "telefono": null,
            "days": null
          }
        ]
      },
      {
        "LOJA": [
          {
            "name": "NOVICOMPU LOJA ",
            "address": "BOLIVAR Y ROCAFUERTE ESQ. LOCAL #0942 - PORTAL DEL PARQUE STO DOMINGO",
            "telefono": null,
            "days": null
          }
        ]
      }
    ]
    


  const items: any = [];
  // for (var i in tiendasJson) {
  tiendasJson.map((value: any) => {
    // let subItems = 
    for (var i in value) {
      const tienda = value[i].map((item: any, index) => {
        return <div key={index} className="ma3 pa3 bg-base">
          <h4>{item.name}</h4>
          <div className={`${stylesGlobals['tiendas-subitem']}`}>
            <Icon id="mpa-store" activeClassName="rebel-pink" />
            <p>Dirección: {item.address}</p>

          </div>
          {item.telefono && (
            <div className={`${stylesGlobals['tiendas-subitem']}`}>
              <Icon id="hpa-telemarketing" activeClassName="rebel-pink" />
              <p>Teléfono: {item.telefono}</p>

            </div>
          )}
          {item.days && (
            <React.Fragment>
              <div className={`${stylesGlobals['tiendas-subitem']}`}>
                <Icon id="sti-clock" activeClassName="rebel-pink" />
                <div>
                  <p>Horario de Atención: </p>
                  {item.days.map((value2: any,index) => {
                    return (
                      <div key={`sub-${index}`}>{value2.day} : {value2.hour}</div>
                    )
                  })}
                </div>

              </div>
            </React.Fragment>
          )}
        </div>
      })
      items.push(
        <div className={`${stylesGlobals['tiendas-item']}`}>
          <h3>{i}</h3>
          <div className="">{tienda}</div>
        </div>
      )
    }

  })

  // }

  return (
    <React.Fragment>

      <div className={`${stylesGlobals['tiendas-main']} w-90`}>
        <h1 className="center">TIENDAS NOVICOMPU</h1>
        <div>
          <div className={`${stylesGlobals['tiendas-mansory']}`}>
            {items}
          </div>
        </div>

      </div>
    </React.Fragment>
  )
}

TiendasPage.schema = {
  title: 'Pagina de tiendas',
  description: 'Pagina de productos de la Ganacell',
  type: 'object',
  properties: {
    tiendas: {
      type: 'string',
      title: 'array de todas las tiendas',
      default:
        `[
                    {
                      "Guayaquil": [
                        {
                          "name": "HIPER NOVICOMPU",
                          "address": "Av. Francisco de Orellana Solar 32. Mz. 110 (Junto a Parrillada del Ñato",
                          "telefono": "(04) 501-0010",
                          "days": [
                            { "day": "LUNES A SÁBADO", "hour": "08:30AM A 7:30PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                          ]
                        },
                        {
                          "name": "NOVICOMPU FRANCISCO DE ORELLANA",
                          "address": "Frente al, Banco Amazonas, Av. Francisco de Orellana, Guayaquil",
                          "telefono": "(04) 600-3791",
                          "days": [
                            { "day": "LUNES A SÁBADO", "hour": "08:30AM A 7:30PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                          ]
                        },
                        {
                          "name": "NOVICOMPU CITY MALL",
                          "address": "Av. Benjamín Carrión Mora, Felipe del Pezo Campuzano C.C. City Mall Local 5 Planta Baja, Guayaquil.",
                          "telefono": "(04) 501-6809",
                          "days": [
                            { "day": "LUNES A SÁBADO ", "hour": "10:00AM A 9:00PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 8:00PM" }
                          ]
                        },
                        {
                          "name": "NOVICOMPU URDESA",
                          "address": "Victor Emilio Estrada, Guayaquil.",
                          "telefono": "(04) 501-6809",
                          "days": [
                            { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                          ]
                        },
                        {
                          "name": "NOVICOMPU MALL DEL SUR",
                          "address": "C. C. Mall del Sur planta baja local 1.",
                          "telefono": "(04) 6044400",
                          "days": [
                            { "day": "LUNES A SÁBADO ", "hour": "10:00AM A 9:00PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 8:00PM" }
                          ]
                        },
                        {
                          "name": "NOVICOMPU ESCOBEDO",
                          "address": "Av. 9 de Octubre entre las calles Chimborazo y Escobedo. Edificio Antón Hermanos.",
                          "telefono": null,
                          "days": [
                            { "day": "LUNES A SÁBADO ", "hour": "10:00AM A 7:30PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 4:00PM" }
                          ]
                        },
                        {
                          "name": "AV. 9 DE OCTUBRE 910 Y RUMICHACA",
                          "address": "Av. 9 de Octubre 910 y Rumichaca.",
                          "telefono": null,
                          "days": [
                            { "day": "LUNES A SÁBADO ", "hour": "10:00AM A 7:30PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                          ]
                        },
                        {
                          "name": "nOVICOMPU CORDOVA",
                          "address": "Av. 9 DE OCTUBRE Y CORDOVA.",
                          "telefono": null,
                          "days": [
                            { "day": "LUNES A SÁBADO ", "hour": "10:00AM A 7:30PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 4:00PM" }
                          ]
                        }
                      ]
                    },
                    {
                      "Quito": [
                        {
                          "name":"NOVICOMPU SHYRIS Y RIO COCA",
                          "address": "Av. Los Shyris y Rio Coca, Quito Ecuador.",
                          "telefono": "(02) 2432599",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:30PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                            ]
                        },
                        {
                          "name":"NOVICOMPU NACIONES UNIDAS",
                          "address": "Av. Naciones Unidas, Entre Japón y Av. de los Shyris (Junto al Banco Bolivariano), Quito.",
                          "telefono": "(02) 2260188",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                            ]
                        },
                        {
                          "name":"NOVICOMPU CC. CARACOL",
                          "address": "C.C El Caracol Local 108",
                          "telefono": "022451600",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 6:00PM" }
                            ]
                        },
                        {
                          "name":"NOVICOMPU AV 6 DE DICIEMBRE",
                          "address": "Quito Norte Av 6 de Diciembre y Colon, Frente al ministerio de telecomunicaciones",
                          "telefono": "2-256-2009",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                            ]
                        },
                        {
                          "name":"NOVICOMPU AV. DEL MAESTRO",
                          "address": "Av. Del Maestro Oe4-202 y calle Quitumbe.",
                          "telefono": "22562009",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 4:00PM" }
                            ]
                        },
                        {
                          "name":"NOVICOMPU CC RECREO",
                          "address": "Av Pedro Vicente Maldonado C. C. El Recreo Isla Tecnológica, Salida 8, Av. Pedro Vicente Maldonado, Quito.",
                          "telefono": "22562009",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "10:00AM A 8:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 7:00PM" }
                            ]
                        },
                        {
                          "name":"NOVICOMPU AV. REPÚBLICA",
                          "address": "Av. República y Av. América Esquina.",
                          "telefono": null,
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                            ]
                        }
                      ]
                    },
                    {
                      "Cuenca": [
                        {
                          "name":"NOVICOMPU CUENCA",
                          "address": "C. C. Mall del Rio.",
                          "telefono": "(07) 288-2829",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "10:00AM A 8:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 8:00PM" }
                            ]
                        }
                      ]
                    },
                    {
                      "Ambato": [
                        {
                          "name":"NOVICOMPU AMBATO",
                          "address": "Lalama N 8-59 entre Juan Benigno Vela y Cevallos, a lado de cooperativa.",
                          "telefono": "(03) 242-4604",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" }
                            ]
                        }
                      ]
                    },
                    {
                      "Manta": [
                        {
                            "name":"NOVICOMPU MANTA",
                          "address": "Calle 13 y Avenida 10.",
                          "telefono": "(05) 2611080",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 3:00PM" }
                            ]
                        }
                        
                      ]
                    },
                    {
                      "Portoviejo": [
                        {
                            "name":"NOVICOMPU PORTOVIEJO",
                          "address": "Avenida Manabi y calle Atahualpa",
                          "telefono": "(05) 2632054",
                          "days": [
                              { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                              { "day": "DOMINGO", "hour": "10:00AM A 3:00PM" }
                            ]
                        },
                        {
                          "name":"NOVICOMPU PORTOVIEJO 2",
                        "address": "PEDRO GUAL ENTRE GARCÍA MORENO FRENTE A MOTO REPUESTO MONTERO",
                        "telefono": null,
                        "days": [
                            { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                            { "day": "DOMINGO", "hour": "10:00AM A 3:00PM" }
                          ]
                      }
                        
                      ]
                    },
                    {
                      "Machala": [
                          {
                              "name":"NOVICOMPU MACHALA",
                            "address": "Av. Buenavista, entre Rocafuerte y Bolivar, Diagonal a la Cámara de Comercio.",
                            "telefono": "072966699",
                            "days": [
                                { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                                { "day": "DOMINGO", "hour": "10:00AM A 3:00PM" }
                              ]
                          },
                          {
                              "name":"NOVICOMPU AV. ROCAFUERTE Y PAÉZ",
                            "address": "Av. Rocafuerte y Paéz.",
                            "telefono": "072966699",
                            "days": [
                                { "day": "LUNES A SÁBADO ", "hour": "09:00AM A 7:00PM" },
                                { "day": "DOMINGO", "hour": "10:00AM A 5:00PM" }
                              ]
                          }
                      ]
                    }
                  ]
                  `
    },
  },
}

export default TiendasPage
