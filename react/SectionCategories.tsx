/* eslint-disable react/jsx-key */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from 'react'
import queryCategories from './graphql/getForSectionCategories.gql'
import { useQuery } from 'react-apollo'
import { Spinner } from 'vtex.styleguide'
import { Icon } from 'vtex.store-icons'
import { useCssHandles } from 'vtex.css-handles'
import stylesMenu from './stylesMenuCategories.css'
// import { useRuntime } from 'vtex.render-runtime'
import ItemMenuMobile from './component/itemMenuMobile/ItemMenuMobile'

const sectionCategories: StorefrontFunctionComponent<{}> = () => {
  const CSS_HANDLES = [
    'menu-categories-mobile',
    'menu-open-categories-mobile',
    'menu-close-categories-mobile',
    'menu-subsubmenu-open',
    'menu-subsubmenu-close',
    'menu-categories-mobile-background',
    'menu-caegories-mobile-main',
  ] as const

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const styles = useCssHandles(CSS_HANDLES)
  const [openOrClose, setOpenOrClose] = useState(false)

  const { loading, error, data } = useQuery(queryCategories)
  // const { navigate } = useRuntime()

  if (loading) return <Spinner />
  if (error) return <div>{error}</div>
  function openOrCloseMenu() {
    setOpenOrClose(!openOrClose)
  }
  // function setStateMenu(isOpen) {
  //   setOpenOrClose(isOpen)
  // }
  // function openOrCloseSubMenu(event: any, hasChildren: boolean, href: string) {
  //   console.log(data.categories);

  //   if (!hasChildren) {
  //     navigate({
  //       to: href,
  //     })
  //     openOrCloseMenu();
  //   }

  //   const ul = event.target.parentElement.parentElement.nextElementSibling as HTMLElement
  //   const nameClassOpen = styles['menu-subsubmenu-open']
  //   const nameClassClose = styles['menu-subsubmenu-close']
  //   if (ul?.classList.contains(styles['menu-subsubmenu-open'])) {
  //     ul.classList.remove(nameClassOpen)
  //     ul.classList.add(nameClassClose)
  //   } else if (ul?.classList.contains(styles['menu-subsubmenu-close'])) {
  //     ul.classList.remove(nameClassClose)
  //     ul.classList.add(nameClassOpen)
  //   }
  //   return event.target
  // }
  // function goCategory(href: string) {
  //   navigate({
  //     to: href,
  //   })
  //   openOrCloseMenu();
  // }

  // function clearAccent(text) {
  //   const textModify = text.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
  //   return textModify.toLowerCase()
  // }

  return (
    <div className={`${styles['menu-caegories-mobile-main']}`}>
      <span className="pr4 pl4" onClick={openOrCloseMenu}>
        <Icon id="mpa-bars" activeClassName="rebel-pink" />
      </span>

      <div
        className={` ${stylesMenu.container} ${
          styles['menu-categories-mobile']
        } ${openOrClose ? stylesMenu['show'] : stylesMenu['hidden']}`}
      >
        <div
          className={`${styles['menu-categories-mobile-background']}
        ${
          openOrClose
            ? stylesMenu['menu-open-mobile']
            : styles['menu-close-categories-mobile']
        }
        `}
        >
          <div className={`${stylesMenu.header}`}>
            <span className={`${stylesMenu.title} t-heading-6 pa3`}>
              Categorías
            </span>
            <button
              className={`${stylesMenu['icon-close']}`}
              onClick={openOrCloseMenu}
            >
              <Icon id="sti-close--line" activeClassName="rebel-pink" />
            </button>
          </div>
          <ul>
            {data.categories.map((value: any, index) => {
              return (
                <ItemMenuMobile
                  item={value}
                  onChangeOpenOrClose={setOpenOrClose}
                  key={index}
                />

                // <li key={index}>
                //   <span className={`${stylesMenu['icon-open-submenu']}`}>
                //     <span className={`${stylesMenu.nameCategory} ${stylesMenu[clearAccent(value.name)]}`} onClick={() => {
                //       goCategory(value.href)
                //     }}>
                //       {value.name}
                //     </span>
                //     {value.hasChildren && (
                //       <button onClick={(e) => {
                //         openOrCloseSubMenu(
                //           e,
                //           value.hasChildren,
                //           value.href
                //         )
                //       }} className="">
                //         <Icon
                //           id="nav-caret--down"
                //           activeClassName="rebel-pink"
                //         />
                //       </button>
                //     )}
                //   </span>
                //   {value.children.length > 0 && (
                //     <ul className={`${styles['menu-subsubmenu-close']}`}>
                //       {value.children.map((valueChild: any, subindex: number) => {
                //         return (
                //           <li key={`sub-${subindex}`}>
                //             <span
                //               onClick={() => {
                //                 goCategory(valueChild.href)
                //               }}
                //             >
                //               {valueChild.name}
                //             </span>
                //           </li>
                //         )
                //       })}
                //     </ul>
                //   )}
                // </li>
              )
            })}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default sectionCategories
