/* eslint-disable react/jsx-key */
/* eslint-disable no-console */
// import React, { useState, useEffect } from 'react'
import React from 'react'
import QUERY_VALUE from './graphql/getCategories.gql'
import { Query, QueryResult } from 'react-apollo'
import styles from './stylesMenu.css'
import { Icon } from 'vtex.store-icons'
import { useRuntime } from 'vtex.render-runtime'

interface IProps {
  isActiveFirstItem: boolean,
  nameFirstItem: string,
  nameIconFirstItem: string,
  urlFirstItem: string,
}

const ItemsHeader: StorefrontFunctionComponent<IProps> = ({ isActiveFirstItem = true, nameFirstItem, nameIconFirstItem, urlFirstItem }) => {
  const visibilityHidden = {
    visibility: 'hidden',
  } as React.CSSProperties;
  const { navigate } = useRuntime()

  // function goCategory(href: string) {
  //   navigate({
  //     to: href,
  //   })
  // }

  function clearAccent(text) {
    const textModify = text.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
    return textModify.toLowerCase()
  }

  return (
    <Query query={QUERY_VALUE}>
      {({ data, loading, error }: QueryResult) => {
        if (error) {
          throw new Error(
            `GraphQL error while rendering Menu rendered Category id ${error}`
          )
        }
        if (loading) {
          return null
        }
        return (
          <React.Fragment>
            {isActiveFirstItem && (
              <li>
                <span className={`${styles.labelMain}`}>
                  <span onClick={() => {
                     navigate({
                      to: urlFirstItem,
                    })
                  }} className={`flex fw9 rebel-pink ${styles['rotate-up-center']}`}>
                    <Icon id={nameIconFirstItem} activeClassName="rebel-pink" />
                    &nbsp; {nameFirstItem}
                  </span>
                </span>
              </li>
            )}
            <li>
              <ul className={`${styles.menu}`}>
                <li>
                  <span className={`${styles.labelMain}`}>
                    <Icon id="mpa-list-items" activeClassName="rebel-pink" />
                    &nbsp; Categorías
                  </span>
                  <ul style={visibilityHidden} className={`${styles.submenu}`}>
                    {data.categories.map((value: any, index: number) => {
                      return (
                        <li key={index}>
                          <a
                            className={styles[clearAccent(value.name)]}
                            href={value.href}
                          >
                            <img
                              src={`/arquivos/${value.name
                                .replace(/\s/g, '-')
                                .toLowerCase()}.svg`}
                              className={`${styles.imgSubitem}`}
                              alt={value.name}
                            />
                            {value.name}
                          </a>
                          {value.children.length > 0 && (
                            <ul className={`${styles.subsubmenu}`}>
                              {value.children.map(
                                (valueChild: any, index: number) => {
                                  return (
                                    <li key={`sub-${index}`}>
                                      <a href={valueChild.href}>
                                        {valueChild.name}
                                      </a>
                                    </li>
                                  )
                                }
                              )}
                            </ul>
                          )}
                        </li>
                      )
                    })}
                  </ul>
                </li>
              </ul>
            </li>
          </React.Fragment>
        )
      }}
    </Query>
  )
}

//This is the schema form that will render the editable props on SiteEditor
ItemsHeader.schema = {
  title: 'Item de Header',
  description: 'Item del header de la tienda',
  type: 'object',
  properties: {
    isActiveFirstItem: {
      title: 'Activar primer item',
      description: 'Activar primer item',
      type: 'boolean',
      default: true,
    },
    nameFirstItem: {
      title: 'Nombre primer item',
      description: 'Nombre del primer item',
      type: 'string',
      default: 'Liquidación',
    },
    nameIconFirstItem: {
      title: 'Nombre Icono primer item',
      description: 'Nombre del icono del primer item',
      type: 'string',
      default: 'sti-discount',
    },
    urlFirstItem: {
      title: 'Url primer item',
      description: 'Url del primer item',
      type: 'string',
      default: '/liquidacion',
    },

  },
}

// export default injectIntl(MyComponent)
export default ItemsHeader
