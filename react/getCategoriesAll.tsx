import { useQuery } from 'react-apollo'
import QUERY_VALUE from './graphql/getCategories.gql'

const HelloData = () => {
  const { loading, error, data } = useQuery(QUERY_VALUE)

  if (loading) {
    return 'Loading…'
  }
  if (error) {
    return `Error ${error}`
  }

  return `Done fetching ${data}`
}

export default HelloData