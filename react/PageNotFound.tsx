import React from 'react'
import { useCssHandles } from 'vtex.css-handles'

const pageNotFound: StorefrontFunctionComponent<{}> = () => {
  const CSS_HANDLES = [
    'page-not-fount-grid',
    'page-not-found-paragrah'
  ]
  const styles = useCssHandles(CSS_HANDLES)
  return (
    <div>
      <div className={`${styles['page-not-fount-grid']}`}>
        <div className={`${styles['page-not-found-paragrah']}`}>
          <h2>No se ha encontrado ningún producto</h2>
          <h3>¿Qué hago?</h3>
          <ul>
            <li>Compruebe los términos introducidos.</li>
            <li>Intenta utilizar una sola palabra.</li>
            <li>Utilice términos genéricos en la búsqueda.</li>
            <li>Busque utilizar sinónimos al término deseado.</li>
          </ul>
        </div>
        <div>
          <img
            src="/arquivos/page_not_found.svg"
            alt="producto o pagina no encontrada"
          />
        </div>
      </div>
    </div>
  )
}

export default pageNotFound
