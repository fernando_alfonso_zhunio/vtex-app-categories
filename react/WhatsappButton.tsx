/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { Icon } from 'vtex.store-icons'
import { useState } from 'react';
import { useEffect } from 'react';

export interface IwhatsappBtn {
    name: string
    urlFacebook: string
    urlWhatsapp: string
    urlInstagram: string

    isActiveBtnFacebook: boolean
    isActiveBtnWhatsapp: boolean
    isActiveBtnInstagram: boolean

    paragramIsActive: boolean
    paragramText: string
    isActiviteInMobile: boolean
}

const WhatsappBtn: StorefrontFunctionComponent<IwhatsappBtn> = (
    { 
      isActiveBtnFacebook,
      isActiveBtnWhatsapp,
      isActiveBtnInstagram, 

      urlWhatsapp, 
      urlFacebook,
      urlInstagram,
      
      name, 
      paragramIsActive, 
      paragramText, 
      isActiviteInMobile
    }: any) => {
    const styles = useCssHandles([
        'whatsapp-btn-main',
        'facebook-btn-wrapper',
        'whatsapp-btn-wrapper',
        'instagram-btn-wrapper',
        'btn-redes-wrapper',
        'whatsapp-btn-img',
        'whatsapp-btn-comment',
        'whatsapp-btn-close',
        'whatsapp-btn-close-transition',
    ]);
    const [pIsActive, setParagramIsActive] = useState(false)
    const [isMobile] = useState<boolean>(window.innerWidth <= 768);
    // let isMobile: boolean = (width );
    if (!isActiviteInMobile && isMobile) {
        return null;
    }

    useEffect(() => {
        if (paragramIsActive) {
            setParagramIsActive(false);
            const interval = setTimeout(() => {
                setParagramIsActive(pIsActive => !pIsActive);
            }, 5000);
            return () => clearTimeout(interval);
        }
        return;
    }, []);

    const closeParagram = () => {
        setParagramIsActive(false);
    };

    const ParagraphWhatsapp = (props: any) => {
        const isActive = props.isActive;
        if (isActive) {
            return (
                <p className={`fadeInRight animated ${styles['whatsapp-btn-comment']}`}>
                    <span
                        onClick={closeParagram}
                        className={`${styles['whatsapp-btn-close']}`}
                    >
                        <Icon id="sti-close--outline" activeClassName="rebel-pink" />
                    </span>
                    <a href={urlFacebook} target="_blank" className="link white">
                        {paragramText.replace("#name#", name)}
                    </a>
                </p>
            );
        }
        return null;
    }
    const BtnFacebook = () => {
        if (isActiveBtnFacebook)
            return (
                <a href={urlFacebook} target="_blank" className={`${styles['facebook-btn-wrapper']} ${styles['btn-redes-wrapper']}`}>
                    <img
                        className={`${styles['whatsapp-btn-img']}`}
                        src="/arquivos/facebook.svg"
                        alt="Facebook Novicompu"
                    />
                </a>
            );
        else return null;
    }

    const BtnInstagram = () => {
        if (isActiveBtnInstagram){
            return (
                <a href={urlInstagram} target="_blank" className={`${styles['instagram-btn-wrapper']} ${styles['btn-redes-wrapper']}`}>
                    <img
                    
                    src="/arquivos/instagram.svg"
                    alt="Instagram Novicompu"
                    />
                </a>
            );
        }
        else return null;
    }

    const BtnWhatsapp = () => {
        if (isActiveBtnWhatsapp)
            return (
                <a href={urlWhatsapp} target="_blank" className={`${styles['whatsapp-btn-wrapper']} `}>
                    <img
                        className={`${styles['whatsapp-btn-img']}`}
                        src="/arquivos/whatsapp.svg"
                        alt="whatsapp Novicompu"
                    />
                </a>
            );
        else return null;
    }
    return (
        <span className={`${styles['whatsapp-btn-main']}`}>
            <BtnInstagram />
            <BtnFacebook />
            <ParagraphWhatsapp isActive={pIsActive} />
            <BtnWhatsapp />
        </span>
    )
}

WhatsappBtn.schema = {
    title: 'Boton de Whatsapp',
    description: 'Pagina de productos de la Ganacell',
    type: 'object',
    properties: {
        paragramIsActive: {
            type: 'boolean',
            default: true,
            description: "Activar parrafo de Whatsapp",
            title: "Activar parrafo de Whatsapp"
        },
        paragramText: {
            type: 'string',
            default: 'Hola #name# si tienes preguntas como y donde comprar escribenos',
            description: "Texto parrafo de Whatsapp (para poner el nombre coloque #name# esto replazara el nombre del campo nombre)",
            title: "Texto parrafo de Whatsapp"
        },
        name: {
            type: 'string',
            default: "Fernando Zhunio",
            description: "Nombre del contacto de Whatsapp",
            title: "Nombre del contacto de Whatsapp"
        },

        urlWhatsapp: {
            type: 'string',
            default: 'https://api.whatsapp.com/send?phone=+593939749166',
            description: "Url de redireccion a whatsapp \nEjemplo: \n 'https://api.whatsapp.com/send?phone=+593939749166'",
            title: "Url de redireccion"
        },
        urlFacebook: {
            type: 'string',
            default: 'https://www.facebook.com/novicompu/',
            description: "Url de redireccion a facebook \nEjemplo: \n 'https://www.facebook.com/novicompu/'",
            title: "Url de redireccion"
        },
        urlInstagram: {
            type: 'string',
            default: 'https://www.instagram.com/novicompu/',
            description: "Url de redireccion a instagram \nEjemplo: \n 'https://www.instagram.com/novicompu/'",
            title: "Url de redireccion"
        },

        isActiviteInMobile: {
            type: 'boolean',
            default: true,
            description: "Activar boton en dispositivos moviles",
            title: "Activar boton en dispositivos moviles"
        },
        isActiveBtnFacebook: {
            type: 'boolean',
            default: true,
            description: "Activar boton de facebook",
            title: "Activar boton de facebook"

        },
        isActiveBtnWhatsapp: {
            type: 'boolean',
            default: true,
            description: "Activar boton de whatsapp",
            title: "Activar boton de whatsapp"
        },
        isActiveBtnInstagram: {
            type: 'boolean',
            default: true,
            description: "Activar boton de instagram",
            title: "Activar boton de instagram"
        },
    }
}

export default WhatsappBtn
